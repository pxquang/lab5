﻿namespace LAB3.Models
{
    public class ShoppingCart
    {
        public List<CartItem> Items { get; set; } = new List<CartItem>();
        public void AddItem(CartItem item)
        {
            var existingItem = Items.FirstOrDefault(i => i.ProductId ==
            item.ProductId);
            if (existingItem != null)
            {
                existingItem.Quantity += item.Quantity;
            }
            else
            {
                Items.Add(item);
            }
        }

        
        public void RemoveItem(int productId)
        {
            Items.RemoveAll(i => i.ProductId == productId);
        }


        public void UpdateItem(CartItem cartItem)
        {
            // Tìm kiếm mục trong giỏ hàng dựa trên ID sản phẩm
            var existingItem = Items.FirstOrDefault(x => x.ProductId == cartItem.ProductId);

            if (existingItem != null)
            {
                // Cập nhật số lượng
                existingItem.Quantity = cartItem.Quantity;
            }
            else
            {
                // Thêm mục mới nếu không tìm thấy
                Items.Add(cartItem);
            }
        }
    }
}
